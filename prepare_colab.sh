#!/bin/bash

PORT=$1

./customize_colab_ssh.py $PORT &&\
ssh-copy-id -i ~/.ssh/colab_id_rsa.pub colab &&\
./send_scripts_to_colab.sh &&\
ssh colab ./disable_ssh_password.sh &&\
ssh colab ./initial_setup.sh
