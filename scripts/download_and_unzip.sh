#!/bin/bash

GDRIVE_FILE_ID=$1
ARCHIVE=archive.zip
TARGET_DIR=$2

if [ -z $GDRIVE_FILE_ID ]
then
    echo "Provide google drive file id"
    exit -1
fi

if [ -z $TARGET_DIR ]
then
    TARGET_DIR=.
fi

./download_file_from_google_drive.py $GDRIVE_FILE_ID $ARCHIVE
unzip -d $TARGET_DIR $ARCHIVE | pv -l > /dev/null && rm $ARCHIVE
