#!/usr/bin/python3

"""
    Usage:
        python download_file_from_google_drive.py FILE_ID FILE_NAME
"""

import os
import sys


file_id = sys.argv[1]
file_name = sys.argv[2]
command = r"""
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id={file_id}' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id={file_id}" -O {file_name} && rm -rf /tmp/cookies.txt
""".format(file_id=file_id, file_name=file_name)

os.system(command)
