#!/bin/bash

echo GPU
nvidia-smi -L
echo

echo CPU
cat /proc/cpuinfo
echo

echo RAM
free -h
echo

echo HARD DRIVE
df -h
echo
