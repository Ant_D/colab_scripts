#!/bin/bash

DIR=$1

python3 -m venv $DIR/venv
source $DIR/venv/bin/activate
pip install --upgrade pip setuptools
pip install -r $DIR/requirements.txt
