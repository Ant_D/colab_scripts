#!/bin/bash

apt update

apt install --yes vim
echo set tabstop=4 > ~/.vimrc
echo set expandtab >> ~/.vimrc

apt install --yes git
git config --global core.editor vim
git config --global alias.co checkout
git config --global alias.ci commit
git config --global alias.br branch
git config --global alias.st status
git config --global alias.staged "diff --staged"

apt install --yes python3
apt install --yes python3-pip
apt install --yes python3-venv
python3 -m pip install --upgrade pip

apt install tmux
apt install pv

#snap install docker
#sudo groupadd -g 10 docker
#usermod -a -G docker $USER
#reboot

