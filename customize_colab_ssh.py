#!/usr/bin/python3

import os
import sys
import re


def parse_port():
    if len(sys.argv) == 1:
        print('Provide port', file=sys.stderr)
        sys.exit(1)
    port_str = sys.argv[1]
    if not port_str.isdigit():
        print('Invalid port', file=sys.stderr)
        sys.exit(1)
    return int(port_str)


def main():
    SSH_CONFIG_PATH = os.path.join(os.getenv('HOME'), '.ssh', 'config')
    port = parse_port()

    colab_config = [
        "Host colab",
        "  Hostname 0.tcp.ngrok.io",
        "  Port {}".format(port),
        "  User root",
        "  IdentityFile ~/.ssh/colab_id_rsa"
    ]
    
    try:
        with open(SSH_CONFIG_PATH, 'r') as f:
            lines = list(map(str.rstrip, f.readlines()))    
    except FileNotFoundError:
        lines = []

    if 'Host colab' in lines:
        colab_begin = lines.index('Host colab')
        colab_end = len(lines)
        for i in range(colab_begin + 1, len(lines)):
            if lines[i].startswith('Host '):
                colab_end = i
                break
        lines = lines[:colab_begin] + lines[colab_end:]

    if lines and lines[-1]:
        lines.append('')
    lines.extend(colab_config)

    with open(SSH_CONFIG_PATH, 'w') as f:
        for line in lines:
            print(line, file=f)


if __name__ == '__main__':
    main()

